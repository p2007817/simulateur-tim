from channel import *
from common_parameters import *

def main():
    channel = Channel()
    for i in range(1000):
        channel.get_next_event()
        event = channel.current_event
        print(channel.current_event.label)
        print("\tstart = ", event.start)
        print("\tend = ", event.end)
        print("-----------------------------")
        channel.update_situation()

    AP_occupation = 0
    mean_CCA_eco = 0
    mean_sleep_eco = 0
    mean_throughput_eco = 0
    mean_CCA_perf = 0
    mean_throughput_perf = 0
    

    AP_occupation = 1 - (channel.participants[0].T_CCA / channel.current_time)
    for i in range(1, nb_perf+1):
        mean_CCA_perf += channel.participants[i].T_CCA / channel.current_time
        mean_throughput_perf += channel.participants[i].sent_frames / channel.participants[i].total_frames
    mean_CCA_perf = mean_CCA_perf / nb_perf
    mean_throughput_perf = mean_throughput_perf / nb_perf
    for i in range(nb_perf+1, nb_perf+nb_eco+1):
        mean_CCA_eco += channel.participants[i].T_CCA / channel.current_time
        mean_sleep_eco += channel.participants[i].T_sleep / channel.current_time
        #mean_throughput_eco += channel.participants[i].sent_frames / channel.participants[i].total_frames
    mean_CCA_eco = mean_CCA_eco / nb_eco
    mean_sleep_eco = mean_sleep_eco / nb_eco
    mean_throughput_eco = mean_throughput_eco / nb_eco
    print("\n\nAP occupation: ", AP_occupation)
    print("Mean sleep time (eco): ", mean_sleep_eco)
    print("Mean CCA time (eco): ", mean_CCA_eco)
    #print("Mean throughput (eco): ", mean_throughput_eco)
    print("Mean CCA time (perf): ", mean_CCA_perf)
    print("Mean throughput (perf): ", mean_throughput_perf)

    f = open("timeline.txt", "w")
    for participant in channel.participants:
        #f.write(get_type)
        f.write(participant.visual_timeline)
        f.write("\n")
    f.close

if __name__ == "__main__":
    main()
