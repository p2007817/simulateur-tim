class Event:
    def __init__(self, start, duration):
        self.start = start
        self.duration = duration
        self.end = self.start + self.duration
        self.label = ""        
