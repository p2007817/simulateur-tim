import random
import math

from event import *
from common_parameters import *

class Station_eco:
    def __init__(self, ID, TIM_group):
        self.ID = ID

        assert(TIM_group < nb_TIM_groups)
        self.TIM_group = TIM_group
        self.time_since_last_beacon = TSBTT * self.TIM_group +1
        
        self.link_capacity = 1
        self.frame_length = 1
        self.frame_frequency = 1/100
        self.total_frames = 0
        self.sent_frames = 0

        self.contention_window = min_contention_window
        self.backoff = -1

        self.is_awake = False
        self.is_competiting = False

        self.T_tx = 0
        self.T_rx = 0
        self.T_CCA = 0
        self.T_sleep = 0
        self.visual_timeline = ""
        
    def get_ID(self):
        return self.ID

    def get_type(self):
        return "eco"
    

    def next_event(self):
        if self.is_awake and self.is_competiting:
            if self.total_frames == self.sent_frames:
                self.is_competiting = False
                self.is_awake = False
                return Event(-1, -1)
            if self.backoff < 0:
                self.backoff = random.randint(1, self.contention_window)
            total_data_length = (self.total_frames - self.sent_frames) * self.frame_length
            duration = total_data_length
            event = Event(self.backoff, duration)
            event.label = "transmission"
            return event
        return Event(-1, -1)
            
                
    
    def update_situation(self, event, participants, current_time):
        self.visual_timeline += "|"
        if self.is_awake or self.time_since_last_beacon + event.start >= TBTT:
            # The STA receives this frame and may react to it.

            ### Updating visual timeline on the period BEFORE the frame ###
            if not self.is_awake:
                self.T_sleep += TBTT - self.time_since_last_beacon
                self.T_CCA += self.time_since_last_beacon + event.start - TBTT
                self.visual_timeline += "z" * math.floor(TBTT - self.time_since_last_beacon)
                self.visual_timeline += "-" * math.floor(self.time_since_last_beacon + event.start - TBTT) 
                self.is_awake = True
            else:
                self.T_CCA += event.start
                self.visual_timeline += "-" * math.floor(event.start)

            ### Reaction to the different types of frame ###
            if event.label == "beacon":
                # Beacon: the STA becomes ready to receive its data
                self.T_rx += event.duration
                self.visual_timeline += "b" * math.floor(event.duration)
                self.is_competiting = True
                self.time_since_last_beacon = event.duration
            elif event.label == "transmission" and self.ID in participants:
                # The STA received its data, it withdraws and returns to sleep
                self.sent_frames = self.total_frames
                self.T_rx += event.duration
                self.visual_timeline += "*" * math.floor(event.duration)
                self.is_competiting = False
                self.is_awake = False
                self.reset_contention_window()
            elif event.label == "collision" and self.ID in participants:
                self.increase_contention_window()
                self.T_tx += event.duration
                self.visual_timeline += "c" * math.floor(event.duration)
            else:
                self.T_CCA += event.duration
                self.visual_timeline += '-' * math.floor(event.duration)
                self.backoff -= event.start + event.duration
                
        else:
            # The STA sleeps through the frame or wakes up in the middle of it.
            # One way or another, it cannot understand what is said, and cannot react accordingly.
            if self.time_since_last_beacon + event.start + event.duration > TBTT:
                self.T_sleep += TBTT - self.time_since_last_beacon
                self.T_CCA += self.time_since_last_beacon + event.start + event.duration - TBTT
                self.visual_timeline += "z" * math.floor(TBTT - self.time_since_last_beacon)
                self.visual_timeline += "-" * math.floor(self.time_since_last_beacon + event.start + event.duration - TBTT)
                self.is_awake = True
            else:
                self.T_sleep += event.start + event.duration
                self.visual_timeline += "z" * math.floor(event.start + event.duration)

        self.total_frames = math.floor(current_time * self.frame_frequency)
        if not event.label == "beacon":
            self.time_since_last_beacon += event.start + event.duration            



    def reset_contention_window(self):
        self.contention_window = min_contention_window
        self.backoff = -1

    def increase_contention_window(self):
        self.contention_window *= 2
        if self.contention_window > max_contention_window:
            self.contention_window = max_contention_window
        self.backoff = -1
