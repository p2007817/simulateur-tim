from AP import *
from channel import *
from common_parameters import *
from event import *
from sta_eco import *
from sta_perf import *
from timestamp import *


def test_timestamp_eq():
    tmp_result = True
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,1,1,1)
    if not t1 == t2:
        print("\t|Timestamp(1,1,1,1,1) != Timestamp(1,1,1,1,1)?")
        tmp_result = False
    t2 = Timestamp(2,1,1,1,1)
    if not t1 != t2:
        print("\t|Timestamp(1,1,1,1,1) == Timestamp(2,1,1,1,1)?")
        tmp_result = False
    t2 = Timestamp(1,2,1,1,1)
    if not t1 != t2:
        print("\t|Timestamp(1,1,1,1,1) == Timestamp(1,2,1,1,1)?")
        tmp_result = False
    t2 = Timestamp(1,1,2,1,1)
    if not t1 != t2:
        print("\t|Timestamp(1,1,1,1,1) == Timestamp(1,1,2,1,1)?")
        tmp_result = False
    t2 = Timestamp(1,1,1,2,1)
    if not t1 != t2:
        print("\t|Timestamp(1,1,1,1,1) == Timestamp(1,1,1,2,1)?")
        tmp_result = False
    t2 = Timestamp(1,1,1,1,2)
    if not t1 != t2:
        print("\t|Timestamp(1,1,1,1,1) == Timestamp(1,1,1,1,2)?")
        tmp_result = False
    return tmp_result

def test_timestamp_add():
    tmp_result = True
    # Adding hours
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,2,3,4,5)
    T = t1+t2
    if not T == Timestamp(2,3,4,5,6):
        print("\t|(" + str(t1) + ") + (" + str(t2) + ") = (" + str(T) + ")?")
        tmp_result = False
    # Adding minutes
    t1 = Timestamp(1,59,59,999,999)
    t2 = Timestamp(0,2,0,0,0)
    T = t1+t2
    if not T == Timestamp(2,1,59,999,999):
        print("\t|(" + str(t1) + ") + (" + str(t2) + ") = (" + str(T) + ")?")
        tmp_result = False        
    # Adding seconds
    t1 = Timestamp(1,1,59,999,999)
    t2 = Timestamp(0,0,2,0,0)
    T = t1+t2
    if not T == Timestamp(1,2,1,999,999):
        print("\t|(" + str(t1) + ") + (" + str(t2) + ") = (" + str(T) + ")?")
        tmp_result = False        
    # Adding ms
    t1 = Timestamp(1,1,1,999,999)
    t2 = Timestamp(0,0,0,2,0)
    T = t1+t2
    if not T == Timestamp(1,1,2,1,999):
        print("\t|(" + str(t1) + ") + (" + str(t2) + ") = (" + str(T) + ")?")
        tmp_result = False
    # Adding us
    t1 = Timestamp(1,1,1,1,999)
    t2 = Timestamp(0,0,0,0,2)
    T = t1+t2
    if not T == Timestamp(1,1,1,2,1):
        print("\t|(" + str(t1) + ") + (" + str(t2) + ") = (" + str(T) + ")?")
        tmp_result = False    
    return tmp_result


def test_timestamp_is_before():
    tmp_result = True
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(0,0,0,0,2)
    if not t2.is_before(t1):
        print("\t|(", t2, ") is not before (", t1, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,2,1,1,1)
    if t2.is_before(t1):
        print("\t|(", t2, ") is before (", t1, ")?")
        tmp_result = False
    return tmp_result

def test_timestamp_is_after():
    tmp_result = True
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,0,0,0,0)
    if not t1.is_after(t2):
        print("\t|(", t1, ") is not after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(0,2,0,0,0)
    if not t1.is_after(t2):
        print("\t|(", t1, ") is not after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(0,0,2,0,0)
    if not t1.is_after(t2):
        print("\t|(", t1, ") is not after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(0,0,0,2,0)
    if not t1.is_after(t2):
        print("\t|(", t1, ") is not after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(0,0,0,0,2)
    if not t1.is_after(t2):
        print("\t|(", t1, ") is not after (", t2, ")?")
        tmp_result = False
        
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(2,1,1,1,1)
    if t1.is_after(t2):
        print("\t|(", t1, ") is after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,2,1,1,1)
    if t1.is_after(t2):
        print("\t|(", t1, ") is after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,2,1,1)
    if t1.is_after(t2):
        print("\t|(", t1, ") is after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,1,2,1)
    if t1.is_after(t2):
        print("\t|(", t1, ") is after (", t2, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,1,1,2)
    if t1.is_after(t2):
        print("\t|(", t1, ") is after (", t2, ")?")
        tmp_result = False
    return tmp_result

def test_timestamp_duration_since():
    tmp_result = True
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,1,1,2)
    T = t2.duration_since(t1)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration since (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,1,2,0)
    T = t2.duration_since(t1)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration since (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,2,0,1)
    T = t2.duration_since(t1)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration since (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,2,0,1,1)
    T = t2.duration_since(t1)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration since (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(2,0,1,1,1)
    T = t2.duration_since(t1)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration since (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,0,0,0)
    T = t2.duration_since(t1)
    if not T == Timestamp(0,0,0,0,0):
        print("\t| Convention not respected")
        tmp_result = False
    return tmp_result

def test_timestamp_duration_until():
    tmp_result = True
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,1,1,2)
    T = t1.duration_until(t2)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration until (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,1,2,0)
    T = t1.duration_until(t2)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration until (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,2,0,1)
    T = t1.duration_until(t2)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration until (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,2,0,1,1)
    T = t1.duration_until(t2)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration until (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(2,0,1,1,1)
    T = t1.duration_until(t2)
    if not t1 + T == t2:
        print("\t|(", t2, ") duration until (", t1, ") is (", T, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t2 = Timestamp(1,1,0,0,0)
    T = t1.duration_until(t2)
    if not T == Timestamp(0,0,0,0,0):
        print("\t| Convention not respected")
        tmp_result = False
    return tmp_result

def test_timestamp_add_duration():
    tmp_result = True
    t1 = Timestamp(1,1,1,1,1)
    t1.add_duration(1, "h")
    if not t1 == Timestamp(2,1,1,1,1):
        print("\t|(", Timestamp(1,1,1,1,1), ") + 1h is (", t1, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t1.add_duration(1, "mn")
    if not t1 == Timestamp(1,2,1,1,1):
        print("\t|(", Timestamp(1,1,1,1,1), ") + 1mn is (", t1, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t1.add_duration(1, "s")
    if not t1 == Timestamp(1,1,2,1,1):
        print("\t|(", Timestamp(1,1,1,1,1), ") + 1s is (", t1, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t1.add_duration(1, "ms")
    if not t1 == Timestamp(1,1,1,2,1):
        print("\t|(", Timestamp(1,1,1,1,1), ") + 1ms is (", t1, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t1.add_duration(1, "us")
    if not t1 == Timestamp(1,1,1,1,2):
        print("\t|(", Timestamp(1,1,1,1,1), ") + 1us is (", t1, ")?")
        tmp_result = False
    t1 = Timestamp(1,1,1,1,1)
    t1.add_duration(7200000000, "us")
    if not t1 == Timestamp(3,1,1,1,1):
        tmp_result = False
    return tmp_result

def main():
    print("\n----- TIMESTAMP -----")
    if test_timestamp_eq():
        print("OK - eq")
    else:
        print("Fail - eq")
    if test_timestamp_add():
        print("OK - add")
    else:
        print("Fail - add")
    if test_timestamp_is_before():
        print("OK - is_before")
    else:
        print("Fail - is_before")
    if test_timestamp_is_after():
        print("OK - is_after")
    else:
        print("Fail - is_after")
    if test_timestamp_duration_since():
        print("OK - duration_since")
    else:
        print("Fail - duration_since")
    if test_timestamp_duration_until():
        print("OK - duration_until")
    else:
        print("Fail - duration_until")
    if test_timestamp_add_duration():
        print("OK - add_duration")
    else:
        print("Fail - add_duration")
        
    
if __name__ == "__main__":
    main()
