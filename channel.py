from AP import *
from sta_perf import *
from sta_eco import *
from event import *
from common_parameters import *


class Channel:
    def __init__(self):
        self.participants = []
        self.create_participants()

        self.current_time = 0
        self.current_event = Event(-1, -1)
        self.current_participant = []
 
    def create_participants(self):
        self.participants.append(AP(0)) # important that the AP is first!
        for i in range(1, nb_perf+1):
            self.participants.append(Station_perf(i))
        for i in range(nb_perf+1, nb_perf+nb_eco+1):
            self.participants.append(Station_eco(i, i % nb_TIM_groups))
            print(i % nb_TIM_groups)
        print("Participants:")
        for u in self.participants:
            print(u.get_ID(), " -> ", u.get_type())
            
    def get_next_event(self):
        print("Current time = ", self.current_time)
        earlier_event = Event(-1, -1)
        earlier_participant = []
        collision = False
        for participant in self.participants:
            event = participant.next_event()
            print(participant.get_ID(), event.start)
            if event.start < 0 :
                # The STA is not in the competition
                # Used for eco STA waiting for the beacon
                pass
            elif earlier_event.start > event.start or earlier_event.start == -1:
                earlier_event.start = event.start
                earlier_event.duration = event.duration
                earlier_event.end = event.end
                earlier_event.label = event.label
                earlier_participant.clear()
                earlier_participant.append(participant.get_ID())
                collision = False
            elif earlier_event.start == event.start: #collision
                if len(earlier_participant) == 1 and earlier_participant[0] and participant.get_type() == "perf":
                    collision = False
                else:
                    earlier_participant.append(participant.get_ID())
                    collision = True
        if collision:
            self.current_event = Event(earlier_event.start, 1)
            self.current_event.label = "collision"
            self.current_participant = earlier_participant
        elif earlier_event.start < 0:
            # No event, wait one time unit
            self.current_event = Event(0, 1)
        else:
            self.current_event = earlier_event
            self.current_participant = earlier_participant
        self.current_time += self.current_event.start + self.current_event.duration

    def update_situation(self):
        for participant in self.participants:
            participant.update_situation(self.current_event, self.current_participant, self.current_time)
        return 0

                
