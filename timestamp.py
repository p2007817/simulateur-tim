class Timestamp:
    def __init__(self, hour, minute, second, millisecond, microsecond):
        self.hour = max(0,hour)
        self.minute = min(max(0,minute),59)
        self.second = min(max(0,second),59)
        self.millisecond = min(max(0,millisecond),999)
        self.microsecond = min(max(0,microsecond),999)

    def __eq__(self, t):
        if self.hour != t.hour:
            return False
        if self.minute != t.minute:
            return False
        if self.second != t.second:
            return False
        if self.millisecond != t.millisecond:
            return False
        if self.microsecond != t.microsecond:
            return False
        return True
        
        
    def __add__(self, t):
        carry = 0
        result = Timestamp(0,0,0,0,0)
        result.microsecond = self.microsecond + t.microsecond
        if result.microsecond >= 1000:
            carry = result.microsecond // 1000
            result.microsecond %= 1000
        else:
            carry = 0
        result.millisecond = self.millisecond + t.millisecond + carry
        if result.millisecond >= 1000:
            carry = result.millisecond // 1000
            result.millisecond %= 1000
        else:
            carry = 0
        result.second = self.second + t.second + carry
        if result.second >= 60:
            carry = result.second // 60
            result.second %= 60
        else:
            carry = 0
        result.minute = self.minute + t.minute + carry
        if result.minute >= 60:
            carry = result.minute // 60
            result.minute %= 60
        else:
            carry = 0
        result.hour = self.hour + t.hour + carry
        return result

    def __str__(self):
        result = str(self.hour) + "h "
        result += str(self.minute) + "m "
        result += str(self.second) + "s "
        result += str(self.millisecond) + "ms "
        result += str(self.microsecond) + "us"
        return result
        

    # Is this timestamp earlier than t?
    # Return True if equal
    def is_before(self, t):
        if self.hour < t.hour:
            return True
        elif self.hour > t.hour:
            return False
        if self.minute < t.minute:
            return True
        elif self.minute > t.minute:
            return False
        if self.second < t.second:
            return True
        elif self.second > t.second:
            return False
        if self.millisecond < t.millisecond:
            return True
        elif self.millisecond > t.millisecond:
            return False
        if self.microsecond < t.microsecond:
            return True
        elif self.microsecond > t.microsecond:
            return False
        return True

    # Is this timestamp later than t?
    # Return True if equal
    def is_after(self, t):
        if self.hour > t.hour:
            return True
        elif self.hour < t.hour:
            return False
        if self.minute > t.minute:
            return True
        elif self.minute < t.minute:
            return False
        if self.second > t.second:
            return True
        elif self.second < t.second:
            return False
        if self.millisecond > t.millisecond:
            return True
        elif self.millisecond < t.millisecond:
            return False
        if self.microsecond > t.microsecond:
            return True
        elif self.microsecond < t.microsecond:
            return False
        return True


    # Compute the duration until t (0 if t is earlier)
    def duration_since(self, t):
        result = Timestamp(0,0,0,0,0)
        if self.is_before(t):
            return result
        
        result.microsecond = self.microsecond - t.microsecond
        if result.microsecond < 0:
            carry = 1
            result.microsecond += 1000
        else:
            carry = 0
        result.millisecond = self.millisecond - t.millisecond - carry
        if result.millisecond < 0:
            carry = 1
            result.millisecond += 1000
        else:
            carry = 0
        result.second = self.second - t.second - carry
        if result.second < 0:
            carry = 1
            result.second += 60
        else:
            carry = 0
        result.minute = self.minute - t.minute - carry
        if result.minute < 0:
            carry = 1
            result.minute += 60
        else:
            carry = 0
        result.hour = self.hour - t.hour - carry
        return result

    # Compute the duration since t (0 if t is later)
    def duration_until(self, t):
        return t.duration_since(self)

    def add_duration(self, duration, unit):
        assert(unit == "h" or unit == "mn" or unit == "s" or unit == "ms" or unit == "us")
        if unit == "h":
            self.hour += duration
        elif unit == "mn":
            self.minute += duration
            if self.minute >= 60:
                carry = self.minute // 60
                self.minute %= 60
                self.add_duration(carry, "h")
        elif unit == "s":
            self.second += duration
            if self.second >= 60:
                carry = self.second // 60
                self.second %= 60
                self.add_duration(carry, "mn")
        elif unit == "ms":
            self.millisecond += duration
            if self.millisecond >= 999:
                carry = self.millisecond // 1000
                self.millisecond %= 1000
                self.add_duration(carry, "s")
        elif unit == "us":
            self.microsecond += duration
            if self.microsecond >= 1000:
                carry = self.microsecond // 1000
                self.microsecond %= 1000
                self.add_duration(carry, "ms")
                
