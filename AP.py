import random
import math

from event import *
from common_parameters import *

# Pas de fenetre de contention qui evolue avec l'AP ?

class AP:
    def __init__(self, ID):
        self.ID = ID
        self.time_since_last_beacon = 0
        self.contention_window = 5
        self.backoff = -1

        self.T_tx = 0
        self.T_rx = 0
        self.T_CCA = 0
        self.T_sleep = 0

        self.visual_timeline = ""

    def get_ID(self):
        return self.ID

    def get_type(self):
        return "AP"
        
    def next_event(self):
        start = -1
        duration = -1
        if TSBTT - self.time_since_last_beacon > 0:
            # The AP waits to send its beacon
            start = int(TSBTT - self.time_since_last_beacon)
            duration = 10
        else:
            if self.backoff == -1:
                self.backoff = random.randint(1, self.contention_window)
            start = self.backoff
            duration = 10
        event = Event(start, duration)
        event.label = "beacon"
        return event
    
    def update_situation(self, event, participants, current_time):
        self.visual_timeline += "|"
        if event.label == "beacon" and self.ID in participants:
            self.time_since_last_beacon = event.duration
            self.backoff = -1
            self.T_tx += event.duration
            self.T_CCA += event.start
            self.visual_timeline += "-" * math.floor(event.start)
            self.visual_timeline += "*" * math.floor(event.duration)
        elif event.label == "collision" and self.ID in participants:
            self.backoff = -1
            self.T_tx += event.duration
            self.T_CCA += event.start
            self.visual_timeline += "-" * math.floor(event.start)
            self.visual_timeline += "c" * math.floor(event.duration)
        else:
            self.time_since_last_beacon += event.start + event.duration
            self.T_tx += event.duration
            self.T_CCA += event.start
            if self.backoff != -1:
                self.backoff -= event.start
            self.visual_timeline += "-" * math.floor(event.start + event.duration)
            
