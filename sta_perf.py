import random
import math

from event import *
from common_parameters import *
class Station_perf:
    def __init__(self, ID):
        self.ID = ID

        self.link_capacity = 1
        self.frame_length = 3
        self.frame_frequency = 1/20
        self.total_frames = 0
        self.sent_frames = 0
        self.previous_frame = -1
        self.next_frame = random.randint(1, math.floor(1/self.frame_frequency))
        
        self.contention_window = min_contention_window
        self.backoff = -1
        
        self.T_tx = 0
        self.T_rx = 0
        self.T_CCA = 0
        self.T_sleep = 0

        self.visual_timeline = ""
        
    def get_ID(self):
        return self.ID

    def get_type(self):
        return "perf"
            
    def next_event(self):
        if self.total_frames == self.sent_frames:
            start = self.next_frame
            duration = self.frame_length
            event = Event(start, duration)
            event.label = "transmission"
            return event
        else:
            if self.backoff < 0:
                self.backoff = random.randint(1,self.contention_window)
            start = self.backoff
            duration = (self.total_frames - self.sent_frames) * self.frame_length
            event = Event(start, duration)
            event.label = "transmission"
            return event

    def update_situation(self, event, participants, current_time):
        self.visual_timeline += "|"
        if event.label == "collision" and self.ID in participants: # The STA collided with another transmission
            # The contention window increases and the backoff is reset
            self.increase_contention_window()
            self.T_CCA += event.start
            self.T_rx += event.duration 
            self.visual_timeline += "-" * math.floor(event.start)
            self.visual_timeline += "c" * math.floor(event.duration)
        elif event.label == "transmission" and self.ID in participants:
            # The transmission is a success: the contention window and the backoff are reset
            self.sent_frames = self.total_frames
            self.reset_contention_window()
            self.T_CCA += event.start
            self.T_rx += event.duration
            self.visual_timeline += "-" * math.floor(event.start)
            self.visual_timeline += "*" * math.floor(event.duration)
        elif event.label == "beacon":
            # Beacons don't change the situation for performance STAs, however they still listen to it (not T_CCA but T_rx)
            self.backoff -= event.start
            self.T_CCA += event.start
            self.T_rx += event.duration
            self.visual_timeline += "-" * math.floor(event.start)
            self.visual_timeline += "-" * math.floor(event.duration)
        else:
            # The event didn't concern the STA.
            # The backoff is updated
            self.backoff -= event.start
            self.T_CCA += event.start + event.duration
            self.visual_timeline += "-" * math.floor(event.start)
            self.visual_timeline += "-" * math.floor(event.duration)
        self.generate_next_frames(current_time)
        return 0

    
    def reset_contention_window(self):
        self.contention_window = min_contention_window
        self.backoff = -1

    def increase_contention_window(self):
        self.contention_window *= 2
        if self.contention_window > max_contention_window:
            self.contention_window = max_contention_window
        self.backoff = -1

    def generate_next_frames(self, current_time):
        while self.next_frame < current_time:
            self.previous_frame = self.next_frame
            self.next_frame += math.floor(1 / self.frame_frequency)
            self.total_frames += 1
